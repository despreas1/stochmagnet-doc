The purpose of this section is to validate the stochastic method. The atomic moment direction of a single spin submitted to an uniaxial anisotropy  and a temperature T is oriented with an angle $\theta_T$ with respect of the easy axis of the anisotropy. The probality that $\theta_T=\theta$ is given by the normalized Boltzmann distribution law :   

$$ P(\theta)=\ds sin(\theta).exp \left (-\frac{k_u}{k_B.T} sin^2(\theta)\right) / P_{max}$$
where
\begin{itemize}
\item $P_{max}$ is chosen such that the maximum of the normalized probability is 1,
\item $k_u$ is the anisotropy energy factor in $J/atom$,
\item $k_B$ is the Boltzmann constant $k_B=13.807 \times 10^{-24} J/K^{-1}$,
\item $T$ is the temperature in $K$.
\end{itemize}

The effective temperature is $T_{eff}=\frac{k_u}{k_B.T}$.\\

The maximum value of the Boltzmann distribution $P_{max}$ is obtained for $\theta=\pi/2$ or $|sin(\theta)|=\sqrt{\frac{1}{2.T_{eff}}}$ which leads to:
\begin{itemize}
\item $T_{eff}\geq \frac{1}{2}$ : $P_{max}=\sqrt{\frac{1}{2.T_{eff}.e}}$
\item $T_{eff} < \frac{1}{2} $ : $P_{max}=e^{-T_{eff}}$
\end{itemize}

The spin is initialized along the easy axis direction of the uniaxial anisotropy $U$.\\
The evolution of the direction $S(t,\varepsilon,s)$ of the magnetic moment of the spin is computed at step $t \in [0,T_p+T_s[$ (with a time step $dt$ for Landau-Lifschistz system) and $T_p$ is the number of precondictioning steps, $T_s$ the number of steps to compute statistic outputs at the simulation $s$ which is the index of the simulation in $[0,N[$ and $\varepsilon$ is the noise rate.\\
The angle of the spin direction with the easy axis is computed only for step $t>=T_p$ by $\theta_t=acos(<S(t,\varepsilon,s),U>_3), \forall (t,s) \in [T_p,T_p+T_s[ \times [0,N[ $.\\
The range of $\theta_t$ is in $[0,\pi]$ which is subdivised into R intervales : $\ds \cup_{i=0}^{i=R-1} [\alpha_i,\alpha_{i+1}[$ with $\alpha_i=i.\pi/R$. The discretized Boltzmann normalized distribution is $P_k=Ak/max_k(A_k)$ where $A_k=card(\{\theta_t \in [\alpha_k,\alpha_{k+1}[\})$.\\

Since the anisotropy energy is symmetric along the easy axis, the probability distribution is reflected and summed about $\pi/2$ since at low temperatures the spin is confined to the upper well ($\theta < \pi/2$)\\

The physical values correspond to Fe material:\\
\begin{tabular}{|l|l|l|}
\hline  
parameter & value & units  \\
\hline
$\tilde \mu_s$  &  $2.22$  & $\mu_B$ \\
$\mu_B$  &  $9.2740 \times 10^{-24}$  & $J.T^{-1}$ \\
$k_u$ & $0.565 \times 10^{-24}$ & $J/atom$\\
$J_{ij}$ & $7050 \times 10^{-24}$ & $J/link$\\
$a$ & $2.866$ & $\Ang$\\
\hline
\end{tabular}\\
\\

The numerical values are:\\
\begin{tabular}{|l|l|}
\hline
parameter & value   \\
\hline
$\lambda$  &  $0.5$  \\
$dt$ & $10^{-3}$ pico-second\\
$T_p$ & $10^4$\\
$T_s$ & $10^8$ \\
$N$ (for LLG) & $32$ \\
$N$ (for MMC) & $1$ \\
$R$ & $1000$\\
\hline
\end{tabular}\\
\\

%%Teff=10
For temperature $T=0.0041K$ and Stratonovich system : \\

\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-10Teff-bd.png} 
    \caption{\href{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-10Teff-bd.png}{$T_{eff}=10, dt=10^{-3} ps$}}
    \label{fig:Teffh10}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-10Teff-sbd.png} 
    \caption{\href{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-10Teff-sbd.png}{$T_{eff}=10, dt=10^{-3} ps$}}
    \label{fig:Teffh10s}
  \end{subfigure}
\end{figure}

With Metropolis Monte Carlo method:
\begin{figure}[H]
  \begin{subfigure}{\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/mmc-boltzmann-Fe-1x1x1-0x0x0p-line-1s-1000000x100000000i-0g-10Teff-bd.png} 
    \caption{\href{Images/Spin/Boltzmann/mmc-boltzmann-Fe-1x1x1-0x0x0p-line-1s-1000000x100000000i-0g-10Teff-bd.png}{$T_{eff}=10$}}
    \label{fig:MCTeff10}
  \end{subfigure}
\end{figure}  

%%Teff=1
For temperature $T=0.041K$ and  Stratonovich system:\\
\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-1Teff-bd.png} 
    \caption{\href{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-1Teff-bd.png}{$T_{eff}=1, dt=10^{-3}ps$}}
    \label{fig:Teffh1}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-1Teff-sbd.png} 
    \caption{\href{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-1Teff-sbd.png}{$T_{eff}=1, dt=10^{-3}ps$}}
    \label{fig:Teffh1s}
  \end{subfigure}
\end{figure}
With Metroplis Monte Carlo algorithm:
\begin{figure}[H]
  \begin{subfigure}{\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/mmc-boltzmann-Fe-1x1x1-0x0x0p-line-1s-1000000x100000000i-0g-1Teff-bd.png} 
    \caption{\href{Images/Spin/Boltzmann/mmc-boltzmann-Fe-1x1x1-0x0x0p-line-1s-1000000x100000000i-0g-1Teff-bd.png}{$T_{eff}=1$}}
    \label{fig:MCTeff1}
  \end{subfigure}
\end{figure}  

%%Teff=0.1
For temperature $T=0.41K$ and Stratonovich system:\\
\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-0.1Teff-bd.png} 
    \caption{\href{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-0.1Teff-bd.png}{$T_{eff}=0.1, dt=10^{-3}$}}
    \label{fig:Teffh0.1}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-0.1Teff-sbd.png} 
    \caption{\href{Images/Spin/Boltzmann/lls-0.5l-0.001ps-boltzmann-Fe-1x1x1-0x0x0p-line-32s-1000000x100000000i-0g-0.1Teff-sbd.png}{$T_{eff}=0.1, dt=10^{-3}$}}
    \label{fig:Teffh0.1s}
  \end{subfigure}
\end{figure}
With Metroplis Monte Carlo algorithm:
\begin{figure}[H]
  \begin{subfigure}{\textwidth}
    \includegraphics[width=0.99\linewidth]{Images/Spin/Boltzmann/mmc-boltzmann-Fe-1x1x1-0x0x0p-line-1s-1000000x100000000i-0g-0.1Teff-bd.png} 
    \caption{\href{Images/Spin/Boltzmann/mmc-boltzmann-Fe-1x1x1-0x0x0p-line-1s-1000000x100000000i-0g-0.1Teff-bd.png}{$T_{eff}=0.1$}}
    \label{fig:MCTeff0.1}
  \end{subfigure}
\end{figure}  


The time of computation on Dahu: \\
\begin{tabular}{|l|l|l|l|}
\hline  
beam & $T_{eff}$ & MPI cores & time  \\
\hline
mmc & 10 & 1 & 53s\\
mmc & 1 & 1 & 53s\\
mmc & 0.1 & 1 & 57s\\
lls & 10 & 32 & 3min 25s\\
lls & 1 & 32 & 3min 26s\\
lls & 0.1 & 32 & 3min 26s\\
llh & 10 & 32 & 6min 25s\\
llh & 1 & 32 & 6min 25s\\
llh & 0.1 & 32 & 7min 01s\\
\hline
\end{tabular}\\
\\

As the computed Boltzman distribution  fits exactly the theorical Boltzmann distribution we can conclude that if the relaxed tile is large enough,
\begin{itemize}
\item the implementation of the stochastic LLG equation is correct
\item the relation between noise rate and temperature is valid.
\item the difficulty of the Landau-Lifschitz system is to estimate the relaxed time and needs small dt which increases the cpu time.
\end{itemize}
