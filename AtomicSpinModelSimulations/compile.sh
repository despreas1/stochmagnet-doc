#!/bin/bash

PREFIX=atomicSpinModelSimulations

#compile the main text
pdflatex $PREFIX.tex
bibtex $PREFIX
pdflatex $PREFIX.tex
pdflatex $PREFIX.tex
okular  $PREFIX.pdf

