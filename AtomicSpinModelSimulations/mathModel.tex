The equation \ref{Eq.LLG} can be rewritten as follow (see \cite{ASLJL}) :
\begin{equation}
  \label{Eq.ALL}
  \ds \frac{d\mu}{dt}=\alpha. A(\mu) h
\end{equation}
where $A: \mathbb{R}^3 \rightarrow M_{3\times3} $ is defined by
\begin{equation}
  \label{Eq.A}
  \ds A(x)=L(x) + \lambda ( x.x^t - x^t.x I )
\end{equation}
and
\begin{equation}
  \label{Eq.L}
  \ds L((x_1,x_2,x_3))=\begin{pmatrix}
    0   & -x_3 &  x_2 \\
    x_3 & 0    & -x_1 \\
    -x_2& x_1  & 0
  \end{pmatrix}
\end{equation}

\subsection{Thermal effect }
In order to model thermal effects, a stochastic perturbation is introduced. Thermal effect can be embedded in the deterministic system \ref{Eq.ALL} by adding a stochastic perturbation to the field h by considering a standard F-Brownian motion which leads to the following stochastic system

\begin{equation}
  \label{Eq.SALL}
  \ds d\mu=\alpha. A(\mu_t) h.dt + \epsilon \alpha A(\mu_t)dW_t   
\end{equation}
 
\subsection{Stratonovich approach}

In order to preserve the norm of S , the Stratonovich approach  consists in adding a new term $K_t . dt$ in the equation \ref{Eq.SALL} and choosing $K_t$ such that $d|\mu_t|=0$.
To compute $K_t$, we use the following It\^{o} theorem:

\begin{thm}
  Let's denote by $W$ a brownian movement in $R^d$ and\\
  \begin{equation}
    \nonumber
    \begin{array}{lll}
      Xt &:&  R \rightarrow R^n \\
         &  & t  \mapsto X_t \\
      b &:& R^+ \times R^n  \rightarrow R^n \\
         & &  (t,X) \mapsto b(t,X) \\
      \sigma &: & R^+ \times R^n     \rightarrow R^n\times R^d \\ 
         & & \ds \partial X_t=b(t,X_t).dt + \sigma(t,X_t) dW_t\\
    \end{array}
  \end{equation}
  if we note by
  \begin{equation}
    \nonumber
    \begin{array}{lll}
      f &:& R^+ \times R^n  \rightarrow R \\
        & &  (t,x) \mapsto f(t,X)\\
    \end{array}
  \end{equation}
  we have $$ df(t,X_t)=\ds \frac{\partial f}{\partial t} dt + \nabla_xf(X_t).dX_t +\frac{1}{2} \sum_{i,j} \frac{\partial^2 f(t,X_t)}{\partial x_i \partial x_j} .d<x_i,x_j>_t $$
  with  $d<x_i,x_j>_t=(\sigma(t,X_t).\sigma(t,Y_t)^T)_{ij} dt$
\end{thm}

If we apply the preceeding theorem to $f(t,X)=<X,X>$ with $d\mu_t$ verifiing $\ds d\mu_t=\alpha. A(\mu_t) h.dt + \epsilon \alpha A(\mu_t)dW_t + K_tdt$, 
$$d|\mu_t|^2=2<\mu_t,\alpha.A(\mu_t).h dt+ \epsilon \alpha A(\mu_t)dW_t + K_tdt>+\frac{1}{2}. \sum_{i,j} 2.\delta_{ij}.\varepsilon^2.\alpha^2.A(\mu_t).A(\mu_t)^T dt$$
As we have from \ref{Eq.LLG} that $<d\mu_t,\alpha.A(\mu_t).h dt+ \epsilon \alpha A(\mu_t)dW_t>=0$, we conclude that
$$d|\mu_t|^2=2.<\mu_t,K_t>.dt + \varepsilon^2.\alpha^2 . tr(A(\mu_t).A(\mu_t)^T)dt$$

After some computations, we have
$$tr(A(\mu_t).A(\mu_t)^T)=2|\mu_t|^2+3\lambda^2|\mu_t|^4-2\lambda^2|\mu_t|^4+\lambda^2|\mu_t|^4$$

So $d|\mu_t|^2=\ds 2. \varepsilon^2.\alpha^2.dt.|\mu_t|^2.\left( 1+\lambda^2.|\mu_t|^2\right)+2.<\mu_t,K_t>.dt$\\

To have a null variation of the norm of $\mu$, we can impose that
$$K_t= - \varepsilon^2.\alpha^2.dt.|\mu_t|^2.\left( 1+\lambda^2.|\mu_t|^2\right) \cdot \mu_t$$

So the equation \ref{Eq.SALL} becomes with $|\mu_t|=1$, 

\begin{equation}
  \label{Eq.STRAT}
  \ds d\mu_t=\alpha. A(\mu_t) h.dt+ \epsilon \alpha A(\mu_t)dW_t - \varepsilon^2.\alpha^2.\left( 1+\lambda^2\right).dt.\mu_t 
\end{equation}

We can  assume now that the noise rate $\varepsilon$ may be  dependant on t. It will be denoted by $\varepsilon_t$.

\subsection{Stratonovich Algorithm}
The algorithm is as follow for the stratonovich or Ito algorithm ($\tau=0$ for Ito algorithm)
\begin{itemize}
\item 1. initializes the seed of the stochastic function
\item 2. initializes to zero, the mean of $\mu, \bar \mu=0$
\item 4. for each trajectory s in [0,S[ do
  \begin{itemize}
  \item 4.0 initializes $t=0$
  \item 4.1 computes $h=h_{ext}^t+h_{exc}^t+h_{ani}^t+h_{dip}^t$ the Zeeman, Heissenberg, anisotropy, and dipolar magnetic fields at time t
  \item 4.2 computes the time step $dt$ (regular for the version 0.03)
  \item 4.3 computes the Brownian motion number $dW_t \sim \ds \sqrt{dt}.\mathcal{N}(0,1)$
  \item 4.4 computes $dh=h.dt+\varepsilon_t.dW_t$
  \item 4.5 computes the normalisation rate $\tau=\varepsilon_t^2.\alpha^2.\left( 1+\lambda^2\right).dt $
  \item 4.6 computes $\mu_{t+dt}=(1-\tau).\mu_t+LL(\mu_t,dh)$
  \item 4.7 normalizes $\mu_{t+dt}$ if needed
  \item 4.8 goes to step 4.1 with $t:=t+dt$, $\mu_t:=\mu_{t+dt}$ while $t<T$
  \end{itemize}
\item 6. go to 4.0 while s < S
\end{itemize}

Numeric errors impose that the step 4.7 must be done. if not, the algorithm does not converge numerically.
$\forall t \in [0,T_{target}], \mu(s,t)$ are computed  by the Stratonovich system algorithm can be illustrated as follow:\\

\input{stratAlgo.tex}


\subsection{Heun Algorithm}
In the Heun method, the predictor step calculates the magnetic moment $\mu_t^p$ from $\mu_t$, $h_t$ and a generated Brownian motion $dW_t$ by integration step:
\begin{eqnarray*}
dW_t &=& \sqrt{dt}.\varepsilon. \mathcal{N}(0,1) \\
h_t &=& h_{ext}(\mu_t)+h_{exc}(\mu_t)+h_{ani}(\mu_t)+h_{dip}(\mu_t)  \\
dh_t &=& dt.h_t+dW_t \\
\Delta \mu_t^p &=& LL(\mu_t,dh_t) \\
Y &=&  \mu_t+\Delta \mu_t^p \\
\forall X , \mu_t^p(X) &=&\frac{1}{|Y(X)|_3} Y(X) \\
\end{eqnarray*}

where $X$ is the particle position.\\
After the predictor step, the effective field must be re-evaluated as the intermediate spin positions have now changed. The Brownian motion is those computed at the predictor step. The corrector step uses the predicted spin position and revised effective field to compute the final spin position at next time $t+dt$:

\begin{eqnarray*}
h^c_t&=&h_{ext}(\mu_t^p)+h_{exc}(\mu_t^p)+h_{ani}(\mu_t^p)+h_{dip}(\mu_t^p) \\
dh^c_t &=& dt.h^c_t+dW_t \\
\Delta \mu_t^c &=& LL(\mu^p_t,dh^c_t) \\
Y &=& \mu_t+\frac{1}{2} \left (\Delta \mu_t^p + \Delta \mu_t^c \right ) \\
\forall X , \mu_{t+dt}(X) &=& \frac{1}{|Y(X)|_3} Y(X) \\
\end{eqnarray*}

\input{heunAlgo.tex}

\subsection{Temperature modeling }

The link between noise rate $\varepsilon$ and the temperature $T$ is modelised by the relation :
\begin{equation}
  \label{Eq.Temperature}
  \ds \varepsilon= \ds \sqrt{\frac{2.\lambda. k_B.T}{\gamma.\mu_s.t_c}}
\end{equation}

With the characteristic magnetic values $t_c=\frac{10^{-30}}{\gamma \mu_0.\mu_B}$, we have
\begin{eqnarray*}
  \ds \varepsilon &=& \ds \sqrt{\frac{k_B\mu_0}{A^3}} \sqrt{\frac{2.\lambda.T}{\tilde \mu_s}}\\
  &\sim& 1.644561  \sqrt{\frac{2.\lambda.T}{\tilde \mu_s}}
  \label{Eq.ATemperature}
\end{eqnarray*}

\newpage

\subsection{Monte Carlo Algorithm}

For having a comparison method, the classical Monte Carlo Metropolis algorithm is used.
It consists in picking up randomly a spin $i$ and its initial spin direction $S_i$ is changed randomly into a new direction $S^{'}_i$. \\
The change in energy $\Delta E = E(S^{'}_i)-E(S_i)$ is computed and the new position is accepted with the probability $P=e^{-\frac{\Delta E}{k_B.T}}$.\\
The procedure is repeated P times where $P$ is the number of spins. Each set of $P$ spin moves is 1 step.
The randomly move of  a spin is either
\begin{itemize}
\item a flip move: $S^{'}_i=-S_i$ or 
\item a gaussian move: $Y_i=S_i+\sigma. \mathcal{N}(0,1), S^{'}_i=\frac{Y_i}{|Y_i|}$ with $\sigma=\frac{2}{25}.\left( \frac{T.k_B}{\mu_B} \right)^{\frac{1}{5}}$
\item a random move: $Y_i=\mathcal{N}(0,1), S^{'}_i=\frac{Y_i}{|Y_i|}$.
\end{itemize}

The algorithm is as follow for 1 simulation where N is the number of steps and P the number of spins:\\

\input{mcAlgo.tex}

\subsection{Beam simulations}
The beam simulation  consists in running S simulations where one simulation is divided in 2 parts:
\begin{itemize}
\item a preconditionning part to reach a stabilization state from initial state $S(t=0)$ for $t_r$ steps. 
\item a statistic part to compute the statistic data  for  $t_s$ steps.
\end{itemize}
It can be illustrated at folow:

\input{beamAlgo.tex}

The parallel simulations with MPI library consist in dividing the S simulations by the number of cores $N_{cores}$.\\
Let's note $\forall c \in [1,N_{cores}], s_c=c. \frac{S}{N_{cores}}$.\\
Each core $c \in [1,N_{cores}]$ runs $s_{c-1}-s_c$  simulations. \\
To satisfy the generated random numbers  per each core to be independent one from the others,  a stochastic jump is applied to the stochastic function.\\
The number of simulated random numbers (a jump) is the number of  random numbers generated by a simulation multiply by the number of simulations done for all cores whose id is less that $c$ : jump=$N_{rng} \cdot s_{c-1}$ \\
If the stochastic function has not an integrated jump function, each core $c$ initializes the seed of its stochastic function by generating $c$ uniform random integer numbers and by taking the last generated random number: \\
$ \forall i \in [0,c[ jump=\mathcal{U}_{int}([0,+\infty[)$\\

