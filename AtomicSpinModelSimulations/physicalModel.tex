The behavior of ferromagnetic particles  at $P_i$ is modeled by the dynamical Laudau-Lifschitz-Gilbert equation (\cite{AEvans}):

\begin{equation}
  \ds \frac{dS_i}{d\tau}=LL(S_i,H^i_{eff}) 
  \label{Eq.LLGP}
\end{equation}

where :
\begin{itemize}
\item $S_i$ is the unit vector representing the direction of the magnetic spin moment of the particle $i$,
\item $\lambda$ is microscopic damping parameter with no unit
\item $\gamma$ is the gyromagnetic ratio express in unit of $T^{-1}.s^{-1}$
\item $H^i_{eff}$ is the net magnetic field on each spin express in units of Tesla ($T=J.A^{-1}.m^{-2}=Kg.A^{-1}.s^{2}$)
\item $LL(\mu,H)=\ds - \frac{\gamma}{1+\lambda^2} \left ( \mu  \wedge H + \lambda \mu \wedge \left ( \mu \wedge H \right ) \right )$, the LLG equation.
\end{itemize}
The atomistic LLG equation describes the interaction of an atomic spin moment of particle  $i$ with an effective magnetic field, which is obtained from the negative first derivative of the complete spin energy, such that:

\begin{equation}
  \ds H^i_{eff}=-\frac{1}{\mu_s}\frac{\partial E}{\partial S_i}
  \label{Eq.EH}
\end{equation}
where $\mu_s$ is the atomic magnetic moment express in units of Joules/Tesla ($J.T^{-1}$) and $E$ the spin energy (or spin Hamiltonian) expressed in units of Joules ($J$).

The energy spin model encapsulates the essential physic magnetic material at the atomic level with the form

\begin{equation}
  \ds E=E_{z}+E_{exc}+E_{ani}+E_{dip}
  \label{Eq.CE}
\end{equation}


\subsection{Zeeman energy }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The Zeeman energy  modelizes the interaction between the particles of the system and an external applied field. The Zeeman energy of a single spin is


\begin{equation}
        E^i_z=- \mu_s <S_i,H_{ext}>_3   
\label{Eq.Ez_1s}
\end{equation}
where $<,>_3$ is the scalar product in a 3D-space.

Because of the relation \ref{Eq.EH}, the Zeeman field expressed in Tesla unit is:
\begin{equation}
H^i_z=H_{ext}
\label{Eq.Hz}
\end{equation}

The total Zeeman energy of the system is the sum of the Zeeman energy of each spin which verifies also the relation \ref{Eq.EH}:
\begin{equation}
        E_z=\sum_i E_z^i=- \sum_i \mu_s <S_i,H_{ext}>_3   
\label{Eq.Ez}
\end{equation}



\subsection{Heisenberg energy }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The Heisenberg or Echange energy is due to the symmetry of the electron wavefunction and the Pauli excusion parameter which governs the orientation of electronic spins in overlapping electron orbitals. Per each spin, the Heisenberg energy is:
\begin{equation}
        E^i_{exc}=- \ds  \sum_{j\neq i} J_{ij} <S_i,S_j>_3
\label{Eq.Eexc_1s}
\end{equation}
where $J_{ij}$ is the exchange interaction between atomic spins of particles $i$ and $j$.\\
The exchange interaction $J_{ij}$ can be considered as  isotropic, meaning that the exchange energy of two spins depends only on their relative orientation, not their direction. In that case $J_{ij}$ is real and positive for ferromagnetic materials (the neighboring spins tend to align parallel), negative for antiferromagnetic materials (the neighboring spins tend to align anti-parallel).\\
The exchange interaction $J_{ij}$ can also be considered as anisotropic. in that case $J_{ij}$ is a $3 \times 3$ matrix:
\begin{equation*}
J_{ij}=
\begin{bmatrix}
J_{xx} & J_{xy} & J_{xz} \\
J_{xy} & J_{yy} & J_{yz} \\
J_{xz} & J_{yz} & J_{zz} \\
\end{bmatrix}
\label{Janisotropic}
\end{equation*}
The expression of the exchange energy of a single spin becomes:
\begin{equation}
        E^i_{exc}=- \ds  \sum_{j\neq i} S_i^t J_{ij} S_j
\label{Eq.Eexc.anisotropic_1s}
\end{equation}

Because of the relation \ref{Eq.EH}, the anisotropic field, expressed in units Tesla, at particle $i$ is
\begin{equation}
H^i_{exc}=\ds \frac{1}{\mu_s} \sum_{j\neq i} J_{ij}.S_j
\label{Eq.Hexc}
\end{equation}

The  Heisenberg energy is the sum if the Heisenberg energy of each spin but in order not to take into account twice the energy of the interaction between spins $i$ and $j$, the sum must be divided by 2 and so the relation  \ref{Eq.EH} is also verified:
\begin{equation}
        E_{exc}=\frac{1}{2} \ds  \sum_i E^i_{exc}=- \ds  \frac{1}{2} \sum_i \sum_{j\neq i} J_{ij} <S_i,S_j>_3
\label{Eq.Eexc}
\end{equation}

\subsection{Anisotropy energy }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
While the exchange energy gives rise to magnetic ordering at the atomic level, the thermal stability of a magnetic material is dominated by the magnetic anisotropy, or preference for the atomic moments to align along a preferred spatial direction.
So he anisotropic energy for a spin depends only on the value of the direction of the atomic spin moment and on spatial directions U:
\begin{equation}
E^i_{ani}=k_U. \Phi_U(S_i)
\label{Eq.Eani_1s}
\end{equation}
In order to satisfy the relation  \ref{Eq.EH}, the anisotropic field is :
\begin{equation}
H^i_{ani}= - \frac{k_U}{\mu_s}. \nabla \Phi_U(S_i)
\label{Eq.Hani}
\end{equation}

The total anisotropy energy of the system is the sum of the anisotropy energy of each spin: 
\begin{equation}
E_{ani}=- \sum_i k_U. \Phi_U(S_i)
\label{Eq.Eani_1s}
\end{equation}

\subsubsection{Uniaxial/Planar anisotropic energy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The simplest form of anisotropy is of the uniaxial type where the magnetic moments prefer to align along a single axis $U$. The uniaxial anisotropy energy of one spin is given by
\begin{equation}
        E^i_{ani}=- \ds  k_u  <S_i,U>^2_3
\label{Eq.Eani.uniaxial_1s}
\end{equation}
where $k_u$ is the anisotropy energy per atom.
To satisfy the general equation,  we have $ \Phi_U(S_i)=- <S_i,U>^2_3 $.  
So the anisotropic field, expressed in units Tesla, at particle $i$ is
\begin{equation}
H^i_{ani}=2.\frac{k_u}{\mu_s} <S_i,U>_3 U 
\label{Eq.Hani.uniaxial}
\end{equation}
Note that if $k_u$ is negative, the anisotropy becomes planar : the magnetic moments prefer to align perpendicular to axis $U$. \\


The total anisotropy energy of the system  which verifies the relation   \ref{Eq.EH}  
\begin{equation}
        E_{ani}=\ds  \sum_i E^i_{ani}= - \ds  \sum_i  k_u <S_i,U>^2_3
\label{Eq.Eani.uniaxial}
\end{equation}

\subsubsection{Cubic anisotropic energy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Materials with a cubic crystal structure has three principal directions which energitically are easy, hard and very hard magnetization direction respectively.
The cubic anisotropy energy of a spin is given by
\begin{equation}
E^i_{ani}=\ds  \frac{k_c}{2} \sum_d <S_i,U_d>_3^4
\label{Eq.Eani.cubic_1s}
\end{equation}
where $k_c$ is the cubic anisotropy energy per atom and $U_d$ the anisotropy directions.
To satisfy the general equation, we have $ \Phi_U(S_i)= \frac{1}{2} \ds \sum_d <S_i,U_d>_3^4 $        
The anisotropic field, expressed in units Tesla, at particle $i$ is
\begin{equation}
H^i_{ani}=-\frac{1}{\mu_s} 2.k_c.\sum_d <S_i,U_d>_3^3 U_d
\label{Eq.Hani.cubic}
\end{equation}


The total anisotropy energy of the system  which verifies the relation   \ref{Eq.EH}  
\begin{equation}
        E_{ani}=\ds  \sum_i E^i_{ani}= \ds  \sum_i  \frac{k_c}{2} \sum_d <S_i,U_d>^4_3
\label{Eq.Eani.cubic}
\end{equation}



\subsection{Dipolar magnetic  energy }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The dipoles magnetic interaction modelized the interaction between all other spins. The  dipoles magnetic energy of a spin $i$ is given by
\begin{equation}
        E^i_{dip}=\ds  \frac{\mu_0 \mu_s^2}{4.\pi} \sum_{j \neq i} \frac{<S_i,S_j>_3}{r_{ij}^3} - \frac{3<S_i,r_{ij}>_3<S_j,r_{ij}>_3}{r_{ij}^5} 
\label{Eq.Edip_1s}
\end{equation}
where $r_{ij}=r_i-r_j$ is the direction vector between the particles $i$ and $j$ in unit $m$. It is in fact the potential energy of the magnetic moment at particle $i$ plugged into a dipolar magnetic field generated by $S_j$.
Because of the relation \ref{Eq.EH}, the dipolar interaction field, expressed in units Tesla, at particle $i$ is
\begin{equation}
H^i_{dip}=-\frac{\mu_0 \mu_s}{4.\pi} \sum_{j\neq i} \frac{1}{r_{ij}^3}S_j - \frac{3<S_j,r_{ij}>_3}{r_{ij}^5} r_{ij}  
\label{Eq.Hdip}
\end{equation}


The  total dipolar  energy of the system is the sum if the dipolar  energy of the spin but in order not to take into account twice the energy of the dipolar interaction betwen spins $i$ and $j$, the sum must be divided by 2 and so the relation  \ref{Eq.EH} is also verified:
\begin{equation}
        E_{dip}=\frac{1}{2} \ds  \sum_i E^i_{dip}=\ds \frac{1}{2}. \sum _i  \frac{\mu_0 \mu_s^2}{4.\pi} \sum_{j \neq i} \frac{<S_i,S_j>_3}{r_{ij}^3} - \frac{3<S_i,r_{ij}>_3<S_j,r_{ij}>_3}{r_{ij}^5} 
\label{Eq.Edip}
\end{equation}



\subsection{Ferromagnetic materials data}

The definition of magnetic physical parameters is:\\
\\
\begin{tabular}{|l|l|l|l|}
\hline
Parameter & Symbol & Value & Units\\
\hline
vacuum Permeability & $\mu_0$ & $4.\pi \times 10^{-7}$ & $ T^2.J^{-1}.m^3=m.kg.s^{-2}.A^{-2}$ \\
Bohr magneton & $\mu_B$ & $9.2740 \times 10^{-24} $ & $J.T^{-1}$ \\
Gyroscopic procession & $\gamma$ & $1.76 \times 10^{11} $ & $T^{-1}.s^{-1}=A.s.kg^{-1}$ \\
Boltzmann constant & $k_B$ & $1.3807 \times 10^{-23}$ &  $J.K^{-1}$\\ 
\hline
\end{tabular}\\
\\
\\
The definition of magnetic physical variable is:\\
\\
\begin{tabular}{|l|l|l|l|}
\hline
Variable & Symbol & Unit & Unit expression \\
\hline
Atomic magnetic moment & $\mu_s$ & Joules/Tesla & $J.T^{-1}$ \\
Unit cell size & $a$ & Angstroms & $\Ang = 0.1 nm=10^{-10}m$ \\
Exchange Energy & $J_{ij}$ & Joules/link & $J$ \\
Uniaxial anisotropic Energy & $k_u$ & Joules/atom & $J$ \\
Cubic anisotropic Energy & $k_c$ & Joules/atom & $J$ \\
External applied field & $H_{ext}$ & Tesla& $T$ \\
Temperature  & $T$ & Kelvin & $K$ \\
Time  & $\tau$ & Seconds & $s$ \\
\hline
\end{tabular}\\

By physical approach, it is possible to quantify the magnetic physical variables.

\subsubsection{Atomic spin moment}
The atomic spin moment $\mu_s$ is related to the saturation magnetization   by :
\begin{equation}
\mu_s=\ds \frac{M_s.a^3}{n_{at}}
\label{Def.Mus}
\end{equation}
where $M_s$ is the magnetization at saturation at temperature $0 K$ expressed in $J.T^{-1}.m^{-3}$, $a$ the unic cell size expressed in $m$ and $n_{at}$ is the number of atoms per unit cell.


\subsubsection{Exchange energy}
For a generic atomistic model with $n_z$ nearest neighbour interactions, the exchange constant is given by the meanfield expression:
\begin{equation}
J_{ij}=\ds \frac{3.k_B.T_c}{\epsilon . n_z}
\label{Def.Jij}
\end{equation}
where $k_b$ is the Boltzamnn constant, $T_c$ the curie temperature, $\epsilon$ is a correction factor dependant of the crystal structure.

\subsubsection{Anisotropy energy}
The atomistic magnetocrystalline anisotropy $k_u$ is derived from the macroscopic anisotropy constant $K_u$ by the expression:
\begin{equation}
k_u=\ds \frac{K_u.a^3}{\epsilon . n_{at}}
\label{Def.ku}
\end{equation}
where $K_u$ is express in units $J.m^{-3}$.
By comparison with the continuous model, we have that $\ds \frac{2.K_u}{M_s}=\frac{2.k_u}{\mu_s}$.

\newpage
\subsubsection{Fer element}
The particles structure of the fer ferromagnetic element $F_e$ is a bcc struture. Each particle $P_i$ is at center of a cube where vertices $P_j$ are its neighboring particles. So, the number of neighboring particles is $n_z=8$
\begin{figure}[H]
  \begin{subfigure}{0.95\textwidth}
    \includegraphics[width=0.95\linewidth]{Images/Materials/ferStructure.png} 
    \caption{\href{Images/Materials/ferStructure.png}{bcc Structure}}
    \label{fig:FerStruct}
  \end{subfigure}
\end{figure}


The size of the $F_e$ cubic struture is $a=2.866  \Ang $.\\
The interatomic spacing $r_{ij}=\frac{\sqrt{3}}{2}.a \sim 2.480 \Ang $. \\
The number of neighboring particles $n_z=8$. \\
The Curie temperature is $T_c=1043K$. \\
The spin wave MF correction $\epsilon=0.766$. \\
The atomic spin moment $\mu_s=2.22 \mu_B$.\\
By the relation  \ref{Def.Jij}, $J_{ij}=\ds \frac{3 \times 1.3807 \times 10^{-23} \times 1043}{0.766 \times 8}=7.0510 \times 10^{-21}$J/link. The anisotropy constant is $k_u=5.65 \times 10^{-25}$.\\
The physical values can be summarized in the table:\\
\\
\begin{tabular}{|l|l|l|l|}
\hline
Parameter & Symbol & value & Unit \\
\hline
Crystal structure & & bcc &   \\
Atomic magnetic moment & $\mu_s$ & $2.22 \times \mu_B=2.058820 \times 10^{-23} $ & $J.T^{-1}$ \\
Unit cell size & $a$ & $2.866 \times 10^{-10}$ & $m$ \\
Interatomic spacing & $r_{ij}$ & $2.480$ & \Ang \\ 
Exchange Energy & $J_{ij}$ & $7.0510 \times 10^{-21}$ & J/link \\
Uniaxial anisotropic Energy & $k_u$ & $5.65 \times 10^{-25}$ & J/atom \\
Spin wave MF correction & $\epsilon$ & $0.766$ & - \\
Curie Temperature  & $T_c$ & $1043$ & $K$ \\
\hline
\end{tabular}\\
\\


\newpage
\subsubsection{Nickel element}
The particles structure of the nickel ferromagnetic element $N_i$ is a fcc struture. Each particle $P_i$ is either at center of a face of a cube or at the summit of the cube. Each particle at the center of a face is connected to the particles at the summit of its face and to other particles at center of faces which are connected to its face.  The number of neighboring particles is $n_z=12$.
\begin{figure}[H]
  \begin{subfigure}{0.95\textwidth}
    \includegraphics[width=0.95\linewidth]{Images/Materials/fcc.png} 
    \caption{\href{Images/Materials/fcc.png}{fcc Structure}}
    \label{fig:FerStruct}
  \end{subfigure}
\end{figure}


The size of the $N_i$ cubic struture is $a=3.524 \Ang $.\\
The interatomic spacing $r_{ij}=\frac{\sqrt{2}}{2}.a \sim 2.492 \Ang $. \\
The number of neighboring particles $n_z=12$. \\
The Curie temperature is $T_c=631K$. \\
The spin wave MF correction $\epsilon=0.790$. \\
The atomic spin moment $\mu_s=0.606 \mu_B$.\\
By the relation  \ref{Def.Jij}, $J_{ij}=\ds \frac{3 \times 1.3807 \times 10^{-23} \times 631}{0.790 \times 12}=2.757 \times 10^{-21}$J/link. The anisotropy constant is $k_u=5.47 \times 10^{-26}$.\\
The physical values can be summarized in the table:\\
\\
\begin{tabular}{|l|l|l|l|}
\hline
Parameter & Symbol & value & Unit \\
\hline
Crystal structure & & fcc &   \\
Atomic magnetic moment & $\mu_s$ & $0.606 \times \mu_B=5.62\times 10^{-24} $ & $J.T^{-1}$ \\
Unit cell size & $a$ & $3.524 \times 10^{-10}$ & $m$ \\
Interatomic spacing & $r_{ij}$ & $2.492$ & \Ang \\ 
Exchange Energy & $J_{ij}$ & $2.757 \times 10^{-21}$ & J/link \\
Uniaxial anisotropic Energy & $k_u$ & $5.47 \times 10^{-26}$ & J/atom \\
Spin wave MF correction & $\epsilon$ & $0.790$ & - \\
Curie Temperature  & $T_c$ & $631$ & $K$ \\
\hline
\end{tabular}\\
\\

\newpage

\subsection{Variables normalization}

The adimensionizing method consists in turning the physical variables with adimentionized variables :
\begin{itemize}
\item $E=\bar e . e $
\item $H=\bar h . h $
\item $\tau=\bar t . t $
\item $X=a . x$ 
\end{itemize}
where $X$ is a physical length value and $x,e,h,t$ are adimensionized variables.
For simplicity, as the cell size is a multiple of Angstrom, we take $a=10^{-10}m=1 \Ang$ and $\mu_s=\tilde \mu_s . \mu_B $.

The adimensionizing LLG equation \ref{Eq.LLGP} gives
$$\ds \frac{dS_i}{dt}=-  \frac{\gamma}{1+\lambda^2}  .\bar t . \bar h . \left (S_i \wedge h_{eff}^i + \lambda \left ( <S_i,h^i_{eff}> S_i - |S_i|^2. h^i_{eff} \right ) \right )$$
As $\mu_s$ is chosen as a multiple of $\mu_B$, we use:\\
from the equation \ref{Eq.Edip}, $\bar e= \mu_0 . \mu_B^2 . a^{-3}$ expressed in $J$ \\
and from the equation \ref{Eq.EH},  $\ds \bar h = \frac{1}{\mu_B} \bar e$  expressed in $T$\\

So the characteristic physical value are  by assuming that $\gamma . \bar t . \bar h = 1 $: 
\begin{eqnarray*}
\bar e &= \mu_0 . \mu_B^2 . a^{-3} & J\\
\bar h &= \mu_0 . \mu_B   . a^{-3} & T\\
\bar t &= \frac{a^3}{\gamma.\mu_0.\mu_B } & s\\
\end{eqnarray*}


The adimensionized correspondance of the equation \ref{Eq.EH} is :
\begin{equation}   
  \ds h^i_{eff}=-\frac{1}{\tilde \mu_s}\frac{\partial e}{\partial S_i}
  \label{Eq.AEH}
\end{equation}


With this characteristic variables the LLG equation becomes:
\begin{equation}
\ds \frac{dS_i}{dt}=\alpha  \left (S_i \wedge h_{eff}^i + \lambda \left ( <S_i,h^i_{eff}> S_i - |S_i|^2. h^i_{eff} \right ) \right )
\label{Eq.LLG}
\end{equation}
with $\alpha=-\frac{1}{1+\lambda^2}$.

By using \ref{Eq.Hz}, the adimensionized external field $h_{ext}$ is defined as:
\begin{equation}
h_{ext}=\ds \frac{H_{ext}}{\bar h}=\ds \frac{H_{ext}. a^3 }{\mu_0 . \mu_B }
\label{Eq.AHz}
\end{equation}

By using \ref{Eq.Hexc},  the adimensionized Heisenberg  field $h_{exc}$ is defined as :   
\begin{equation}
h_{exc}=\ds \frac{H_{exc}}{\bar h}=\ds \frac{1}{\tilde \mu_s}\sum_{ij} \tilde J_{ij} S_i
\label{Eq.AHexc}
\end{equation}
with 
\begin{equation}
\tilde J_{ij}=\ds \frac{J_{ij}.a^3}{\mu_B^2.\mu_0}
\label{Eq.AJij}
\end{equation}

By using \ref{Eq.Hani}, the adimensionized anisotropy field $h_{ani}$ is defined as :
\begin{equation}
h_{ani}=\ds \frac{H_{ani}}{\bar h}=\ds -\frac{1}{\tilde \mu_s }\tilde k_U \nabla \Phi_U(S_i) 
\label{Eq.AHani}
\end{equation}
with
\begin{equation}
\tilde k_u= \frac{k_U.a^3}{\mu_B^2 .\mu_0}  
\label{Eq.AKu}
\end{equation}

By using \ref{Eq.Edip}, the adimensionized dipolar Energy $e_{dip}$ is defined as :
\begin{eqnarray}
e_{dip}&=&\ds \frac{E_{dip}}{\bar e} \\
&=&\frac{\mu_0\mu_B^2.\tilde \mu_s^2}{4.\pi a^3} \frac{a^3}{\mu_0.\mu_B^2} \sum_{j\neq i} \frac{1}{\tilde r_{ij}^3}<S_i,S_j> - \frac{3<S_j,\tilde r_{ij}>_3}{\tilde r_{ij}^5} < \tilde r_{ij},S_i> \\
&=&\ds \frac{\tilde \mu_s^2}{4.\pi}  \sum_{j\neq i} \frac{1}{\tilde r_{ij}^3}<S_i,S_j> - \frac{3<S_j,\tilde r_{ij}>_3}{\tilde r_{ij}^5} <S_i,\tilde r_{ij}> 
\label{Eq.AEdip}
\end{eqnarray}
with $\tilde r = \ds \frac{r}{a}$ expressed in $\Ang$

By the relation   \ref{Eq.AEH} , we conclude that the adimensionized dipolar field is
\begin{equation}
h^i_{dip}=\ds - \frac{\tilde \mu_s}{4.\pi}  \sum_{j\neq i} \frac{1}{\tilde r_{ij}^3}S_j - \frac{3<S_j,\tilde r_{ij}>_3}{\tilde r_{ij}^5} \tilde r_{ij} 
\label{Eq.AHdip}
\end{equation}

The characteristic values do not depend on the material:\\
\\
\begin{tabular}{|l|l|l|l|l|}
\hline
Characteristic Values & expression & value & units  \\
\hline
time   & $\bar t$ &  $\frac{a^3}{\gamma.\mu_0.\mu_B }$ & $4.8754 \times 10^{-13}$  & $s$ \\
energy & $\bar e$ & $\mu_B^2 . \mu_0 . 10^{-30} $ & $1.0808\times 10^{-22}$ & $J$ \\
field  & $\bar h$ & $\mu_B   . \mu_0 . 10^{-30} $ & $11.6541$ & $T$ \\
\hline
\end{tabular}\\
\\
The adimensionized values for the Fe material are :\\
\\
\begin{tabular}{|l|l|l|}
\hline
Adimensionized Values & expression & value  \\
\hline
$\tilde J_{ij}$  & $\ds \frac{J_{ij}.10^{30}}{\mu_B^2.\mu_0}$ &  $65.2388$ \\
$\tilde k_u$ &   $\ds \frac{k_U.  10^{30}}{\mu_B^2 .\mu_0}$ & $0.00522761$ \\
\hline
\end{tabular}\\


With respect of the small value of characteristic energy, it is more pertinent to use the adimensionized formulation of the system when energy is used for using convergence criterium because its variation is too small to be detected by computation.\\

This adimensionized methods based on magnetic physical constants is usefull for implementing multi-materials systems and to compare adimensionized results obtained from different materials. 
