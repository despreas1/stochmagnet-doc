set title 'Time Curie Epsilon for StratonovichNormalizedSystem/ConstantNoiseRateFunction ' 
set terminal png size 800,500 enhanced background rgb 'white'
set style increment default 
set style data linespoints 
set autoscale x
set autoscale y
set xtic auto
set ytic auto
set xlabel 'epsilon'
set ylabel 'sqrt(sum_k E_s(|bar mu^s_k\(epsilon,T)|)^2)'
set output './/curie-temperature-dahu.png'
plot 'curie-temperature-dahu-dt5e-3.ct' using 1:2 title 'curie-temperature dt=5.e-3' with lines , 'curie-temperature-dahu-dt5e-4.ct' using 1:2 title 'curie-temperature dt=5.e-4' with lines 
