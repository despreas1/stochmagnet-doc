set title 'Curie Vs Time Curie Epsilon for StratonovichNormalizedSystem/ConstantNoiseRateFunction ' 
set terminal png size 800,500 enhanced background rgb 'white'
set style increment default 
set style data linespoints 
set autoscale x
set autoscale y
set xtic auto
set ytic auto
set xlabel 'epsilon'
set ylabel 'E'
set output './/curie-temperatures-dahu.png'
plot 'time-curie-temperature-dahu-dt5e-4.tct' using 1:2 title 'TCT dt=5.e-4' with lines, 'time-curie-temperature-dahu-dt5e-3.tct' using 1:2 title 'TCT dt=5.e-3' with lines , 'curie-temperature-dahu-dt5e-3.ct' using 1:2 title 'CT dt=5.e-3' with lines,  'curie-temperature-dahu-dt5e-4.ct' using 1:2 title 'CT dt=5.e-4' with lines
