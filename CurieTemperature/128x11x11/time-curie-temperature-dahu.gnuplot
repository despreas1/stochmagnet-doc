set title 'Time Curie Epsilon for StratonovichNormalizedSystem/ConstantNoiseRateFunction ' 
set terminal png size 800,500 enhanced background rgb 'white'
set style increment default 
set style data linespoints 
set autoscale x
set autoscale y
set xtic auto
set ytic auto
set xlabel 'epsilon'
set ylabel '\sqrt{\sum_k E_t(|\bar \mu_k(epsilon,t>T)|)^2}
set output './/time-curie-temperature-dahu.png'
plot 'time-curie-temperature-dahu-dt5e-4.tct' using 1:2 title 'dt=5.e-4' with lines, 'time-curie-temperature-dahu-dt5e-3.tct' using 1:2 title 'dt=5.e-3' with lines
