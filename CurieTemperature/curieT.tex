%% include the style of document
%% =============================

% \input{headerRevTex4}

% \input{headerFrArticle}
\input{headerEnArticle}

%% include the bibbliography packages
\input{headerBib}

%% include the diagram commands
%% ============================
\input{headerDiagram}


%% include the math commands
%% =========================
\input{headerMath}

%% packages configuration
%% ======================
%% \setlength{\columnseprule}{0.5pt}


%% Title of paper
\title{Curie temperature simulations }
\date{2024 June}
\author{Stéphane Despreaux, Grenoble Alpes University \thanks{Laboratoire Jean Kuntzmann}\\
Stéphane Labbé, Sorbone University \thanks{Laboratoire Jacques-Louis Lions}\\
Jérôme Lelong, Grenoble Alpes University}


%% start of document
\begin{document}


%%create the title
\maketitle

The purpose of this article is to simulate the behavior of a  ferromagnetic particles with a stochastic perturbation in order to regain the Curie temperature. 

\section{Modeling}

The behavior of ferromagnetic particles submitted is usually modelled by the deterministic dynamical Laudau-Lifschitz system:

\begin{equation}
  \label{Eq.LL}
  \ds \frac{dM}{d\tau}=LL(M,H) 
\end{equation}

where :
\begin{itemize}
\item $LL(M,H)=\ds - |\gamma| .\mu_0 \left ( M  \wedge H(M) + \frac{\beta}{M_s} M \wedge \left ( M \wedge H(M) \right ) \right )  $ 
\item $M$ the magnetization
\item $H$ total magnetic field
\item $E$ the energy of the system with the relation  $ \frac{dE}{dM}=-B=-\mu_0 H(M) $
\end{itemize}

The physical constants are:\\

\begin{tabular}{|l|l|l|l|}
\hline
vacuum permeability & $\mu_0$ & $4.\pi.1.e^{-7}$ & $ m.kg.s^{-2}.A^{-2}$ \\
gyroscopic procession & $\gamma$ & $-1.76e^{-11}$ & $A.s.kg^{-1}$ \\
absorption parameter & $\beta$ & $\frac{1}{2}$ &  \\
Magnetization at saturation& $M_s$ & $1.4e^{6}$ & $A.m^{-1}$\\
\hline
\end{tabular}\\
\\
The equation is adimensionized by the following parameters:\\
\begin{tabular}{lll}
$M$ & = & $M_s. \mu $\\
$H$ & = & $M_s. h$ \\
$E$ & = & $\bar E e$ \\
$\tau$ & =& $\frac{1}{|\gamma| \mu_0 M_s }   t$ \\
\end{tabular}\\

So $\mu$ verifies:
\begin{equation}
  \label{Eq.ll}
  \frac{d\mu}{dt}=- (\mu \wedge h(\mu) + \beta \mu \wedge \left ( \mu \wedge h(\mu) \right ) 
\end{equation}  

The normalized energy is
\begin{eqnarray*}
\frac{dE}{dM} &= & - \mu_0. H  \\
\frac{\bar E de}{M_s d\mu } &= & - \mu_0. M_s h \\
\frac{de}{d\mu } &= & - \frac{M_s^2 \mu_0 }{\bar E} h
\end{eqnarray*}
So to have $ \frac{de}{d\mu } =-h$ we have to adimensionized the energy by $\bar E= \mu_0. M_s^2 $ expressed in $J/m^3$ or $kg.m^2.s^{-2}/m^3$

In the program the preceeding expression is replaced by
$$ ll(\mu,h)=- (\mu \wedge h(\mu) + \beta \left ( <\mu,h> \mu - |\mu|^2. h \right )  $$ 


The equation \ref{Eq.LL} can be rewritten as follow (see \ref{A01SLJL}) ($\alpha=-1$):
\begin{equation}
  \label{Eq.ALL}
  \ds \frac{d\mu}{dt}=\alpha. A(\mu) h
\end{equation}
where $A: \mathbb{R}^3 \rightarrow M_{3\times3} $ is defined by
\begin{equation}
  \label{Eq.A}
  \ds A(x)=L(x) + \beta ( x.x^t - x^t.x I )
\end{equation}
and
\begin{equation}
  \label{Eq.L}
  \ds L((x_1,x_2,x_3))=\begin{pmatrix}
    0   & -x_3 &  x_2 \\
    x_3 & 0    & -x_1 \\
    -x_2& x_1  & 0
  \end{pmatrix}
\end{equation}

The particles are submitted to
\begin{itemize}
\item an external field $h_{ext}(t,P_i)$ time and space dependent on the network of $P$ points located at $P_i$
\item a demagnetized influence from the other particles $h_d(\mu,P_i)$
\item an exchange interaction between neighbored particles $h_h(\mu)$
\end{itemize}


The relation of the energy E with respect of the magnetic field h at point $P_i$ and the magnetic moment $\mu_i$ is $\frac{dE}{d\mu}=- \mu_0 h $.

\subsection{Zeeman energy }
The Zeeman energy and the Zeeman magnetic excitation is given by :
\begin{itemize}
\item $e_z(t)=- \sum_j <\mu_j(t),h_{ext}(t)>_P$
\item $h_z(t,P_i)=h_{ext}(t,P_i)$
\end{itemize}

The Zeeman energy is minimum when the magnetic moment of the particle is oriented along $h_{ext}$.

Note that the Zeeman magnetic operator is a constant with respect of $\mu$, so the energy can be also computed as $e=-<h_{ext},\mu>_P$

\subsection{Heisenberg energy }

The Heisenberg energy and   magnetic excitation is given by the Heisenberg formula  :

\begin{itemize}
\item $e_h=- \ds  \frac{1}{2}\sum_{i,j} J_{ij} <\mu_i,\mu_j>_3$
\item $h_h(P_i)= \sum_{j} J_{ij} \mu_j $
\end{itemize}
Note that the Heisenberg magnetic operator is linear with respect of $\mu$, so the energy can be also computed as $E=-\frac{1}{2} <h_{h},\mu>_P$

The Heisenberg operator modelized the exchange phenomena between particles with coefficent A expressed in $J.m^{-1}=kg.m^2.s^{-2}.m^{-1}$.


The adimensionized of A is $\bar A=\frac{A}{\mu_0.M_s^2.L^2}$ where L is the reference length which is the distance between 2 particles.\\
the relation between adimensionized coefficient $J_{ij}$ and $\bar A$ is given by :
$$J_{ij}=\bar A . \frac{1}{r_{ij}^2} $$

\subsection{Anisotropy energy }

The uniaxial anisotropy energy and magnetic excitation is given by the formula $E_a= - \frac{1}{2} \frac{K}{\mu_0.M_s^2} \sum_{j} (<M_j,U>_3^2-<M_j,M_j>_3^2)$.\\
The adimensionized energy is $e_a=-\frac{1}{2} \bar K . \sum_j(<\mu_j,U>_3^2-1)$ with $\bar K = \frac{K}{\mu_0 M_s^2}$ 
\begin{itemize}
\item $e_a=\ds  - \frac{1}{2} \bar K  \sum_j ( <\mu_j,U>^2_3-1)$
\item $h_a(P_i)= \bar K  (<\mu_i,U>_3 U - \mu_i ) $      
\end{itemize}

The planar anisotropy energy and magnetic excitation is given by the formula $E_a= \frac{1}{2} \frac{K}{\mu_0.M_s^2} \sum_{j} <M_j,U>_3^2$.\\
The adimensionized energy is $e_a=\frac{1}{2} \bar K . \sum_j(<\mu_j,U>_3^2)$ with $\bar K = \frac{K}{\mu_0 M_s^2}$ 
\begin{itemize}
\item $E_a= \ds \frac{1}{2} \bar K  \sum_{j} <\mu_j,U>_3^2 $
\item $h_h(P_i)= - \bar K <\mu_i,U>_3 U $
\end{itemize}

Note that the planar or uniaxial  operator is linear with respect of $\mu$, so the energy can be also computed as $E=-\frac{1}{2} <h_a,\mu>_P$

\subsection{Demagnetized  energy }

The magnetic interaction between particles is modeled by a demagnetized operator  which is defined in each particle $P_i$ of the domain by the formula:
\begin{itemize}
\item $e_d=        \ds \frac{1}{8\pi} \sum_{i \neq j} \left ( \frac{<\mu_i,\mu_j>_3}{|r_{ij}|^3} - 3 \frac{<\mu_i,r_{ij}>_3.<\mu_j,r_{ij}>_3}{|r_{ij}|^5} \right ) $
\item $h_d(P_i)= - \ds \frac{1}{4\pi} \sum_{i \neq j} \left ( \frac{\mu_j}{|r_{ij}|^3} - 3 \frac{r_{ij}.<\mu_j,r_{ij}>_3}{|r_{ij}|^5} \right ) $
\item $r_{ij}=P_i-P_j$
\end{itemize}

Note that the demagnetized magnetic operator is linear with respect of $\mu$, so the energy can be also computed as $E=-\frac{1}{2} <h(\mu),\mu>_P$

\subsection{Thermal effect }
In order to model thermal effects, a stochastic perturbation is introduced. Thermal effect can be embedded in the deterministic system \ref{Eq.ALL} by adding a stochastic perturbation to the field h by considering a standard F-Brownian motion which leads to the following stochastic system

\begin{equation}
  \label{Eq.SALL}
  \ds d\mu=\alpha. A(\mu_t) h.dt + \epsilon \alpha A(\mu_t)dW_t   
\end{equation}


\section{Ito approach}
The preceeding system is not physically consistent as it does not preserve the norm of $\mu$ which must be invariant equals to 1. To preserve the invariance of the norm of $\mu$, the It\^o approach will consist to normalize $\mu$ at each iteration:

\begin{equation}
  \label{Eq.Ito}
  \begin{cases}
    \ds dY_t=\alpha. A(\mu_t) h .dt + \epsilon \alpha A(\mu_t)dW_t \\
    \mu_t=\ds \frac{Y_t}{|Y_t|}\\
    dW_t \sim \sqrt{dt} \mathcal{N}(0,1)
  \end{cases}\,
\end{equation}


\section{Stratonovich approach}

To preserve the invariance of the norm of $\mu$, the It\^o approch et modified in the Stratonovich approach.
It consists in adding a new term $K_t . dt$ in the equation \ref{Eq.SALL} and choosing $K_t$ such that $d|\mu_t|=0$.
To compute $K_t$, we use the following It\^{o} theorem:

\begin{thm}
  Let's denote by $W$ a brownian movement in $R^d$ and\\
  \begin{equation}
    \nonumber
    \begin{array}{lll}
      Xt &:&  R \rightarrow R^n \\
         &  & t  \mapsto X_t \\
      b &:& R^+ \times R^n  \rightarrow R^n \\
         & &  (t,X) \mapsto b(t,X) \\
      \sigma &: & R^+ \times R^n     \rightarrow R^n\times R^d \\ 
         & & \ds dX_t=b(t,X_t).dt + \sigma(t,X_t) dW_t\\
    \end{array}
  \end{equation}
  if we note by
  \begin{equation}
    \nonumber
    \begin{array}{lll}
      f &:& R^+ \times R^n  \rightarrow R \\
        & &  (t,x) \mapsto f(t,X)\\
    \end{array}
  \end{equation}
  we have $$ df(t,X_t)=\ds \frac{\partial f}{\partial t} dt + \nabla_xf(X_t).dX_t +\frac{1}{2} \sum_{i,j} \frac{\partial^2 f(t,X_t)}{\partial x_i \partial x_j} .d<x_i,x_j>_t $$
  with  $d<x_i,x_j>_t=(\sigma(t,X_t).\sigma(t,Y_t>^T)_{ij} dt$
\end{thm}

If we apply the preceeding theorem to $f(t,X)=<X,X>$ with $d\mu_t$ verifiing $\ds d\mu_t=\alpha. A(\mu_t) h.dt + \epsilon \alpha A(\mu_t)dW_t + K_tdt$, 
$$d|\mu_t|^2=2<\mu_t,\alpha.A(\mu_t).h dt+ \epsilon \alpha A(\mu_t)dW_t + K_tdt>+\frac{1}{2}. \sum_{i,j} 2.\delta_{ij}.\varepsilon^2.\alpha^2.A(\mu_t).A(\mu_t)^T dt$$
As we have from \ref{Eq.LL} that $<d\mu_t,\alpha.A(\mu_t).h dt+ \epsilon \alpha A(\mu_t)dW_t>=0$, we conclude that
$$d|\mu_t|^2=2.<\mu_t,K_t>.dt + \varepsilon^2.\alpha^2 . tr(A(\mu_t).A(\mu_t)^T)dt$$

After some computations, we have
$$tr(A(\mu_t).A(\mu_t)^T)=2|\mu_t|^2+3\beta^2|\mu_t|^4-2\beta^2|\mu_t|^4+\beta^2|\mu_t|^4$$

So $d|\mu_t|^2=\ds 2. \varepsilon^2.\alpha^2.dt.|\mu_t|^2.\left( 1+\beta^2.|\mu_t|^2\right)+2.<\mu_t,K_t>.dt$\\

To have a null variation of the norm of $\mu$, we can impose that
$$K_t= - \varepsilon^2.\alpha^2.dt.|\mu_t|^2.\left( 1+\beta^2.|\mu_t|^2\right) \cdot \mu_t$$

So the equation \ref{Eq.SALL} becomes with $|\mu_t|=1$, 

\begin{equation}
  \label{Eq.STRAT}
  \ds d\mu_t=\alpha. A(\mu_t) h.dt+ \epsilon \alpha A(\mu_t)dW_t - \varepsilon^2.\alpha^2.\left( 1+\beta^2\right).dt.\mu_t 
\end{equation}

We can  assume now that the noise rate $\varepsilon$ may be  dependant on t. It will be denoted by $\varepsilon_t$.

\newpage
\section{Algorithm}
The algorithm is as follow for the stratonovich or Ito algorithm ($\tau=0$ for Ito algorithm)
\begin{itemize}
\item 1. initializes the seed of the stochastic function
\item 2. initializes to zero, the mean of $\mu, \bar \mu=0$
\item 4. for each trajectory s in [0,S[ do
  \begin{itemize}
  \item 4.0 initializes $t=0$
  \item 4.1 computes $h=h_z^t+h_h^t+h_d^t$ the Zeeman,Heisenberg and Demagnetized magnetic fields at time t
  \item 4.2 computes the time step $dt$ (regular for the version 0.03)
  \item 4.3 computes the Brownian motion number $dW_t \sim \ds \sqrt{dt}.\mathcal{N}(0,1)$
  \item 4.4 computes $B=h.dt+\varepsilon_t.dW_t$
  \item 4.5 computes the normalisation rate $\tau=\varepsilon_t^2.\alpha^2.\left( 1+\beta^2\right).dt $
  \item 4.6 computes $\mu_{t+dt}=(1-\tau).\mu_t+LL(\mu_t,B)$
  \item 4.7 normalizes $\mu_{t+dt}$ if needed
  \item 4.8 goes to step 4.1 with $t:=t+dt$, $\mu_t:=\mu_{t+dt}$ while $t<T$
  \end{itemize}
\item 6. go to 4.0 while s < S
\end{itemize}

Numeric errors impose that the step 4.7 must be done. if not, the algorithm does not converge numerically.
The beam simulation  can be illustrated as follow:

\input{beamAlgo.tex}

Whereas the $\mu_t$ simulation by the Stratonovich system algorithm can be illustrated as follow:

\input{stratAlgo.tex}

\newpage
\section{Curie Temperature simulation}

\input{128x11x11-test.tex}

%%input{line-test}

\bibliographystyle{plain}
\bibliography{bibliography}

%% end of document
\end{document}
