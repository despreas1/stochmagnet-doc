#!/bin/bash
#script to compile the tex file on ubuntu

TEX_FILE=MOSICOF2024

#cd images
#for f in `ls *.eps` 
#do
#	echo "convert $f to pdf file "
#	fn=`basename $f`
#	ext="${fn##*.}"
#	prefix="${fn%.*}"
#	dst=${prefix}-eps-converted-to.pdf
#	epstopdf --outfile $dst $fn && echo "convert into $dst has succeeded" 
#done	
#cd ..

#tex -> latex
pdflatex $TEX_FILE.tex
#re-run for links settings
pdflatex $TEX_FILE.tex

#cleaning built files
rm ${TEX_FILE}.aux
rm ${TEX_FILE}.log
rm ${TEX_FILE}.nav
rm ${TEX_FILE}.snm
rm ${TEX_FILE}.toc
rm ${TEX_FILE}.out


#show the result
evince ${TEX_FILE}.pdf

