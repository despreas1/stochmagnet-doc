The behavior of ferromagnetic particles  at $P_i$ is modeled by the dynamical Laudau-Lifschitz-Gilbert equation (\cite{AEvans}):

\begin{equation}
  \ds \frac{dS_i}{d\tau}=LL(S_i,H^i_{eff}) 
  \label{Eq.LLGP}
\end{equation}

where :
\begin{itemize}
\item $S_i$ is the unit vector representing the direction of the magnetic spin moment of the particle $i$,
\item $\lambda$ is microscopic damping parameter with no unit
\item $\gamma$ is the gyromagnetic ratio express in unit of $T^{-1}.s^{-1}$
\item $H^i_{eff}$ is the net magnetic field on each spin express in units of Tesla ($T=J.A^{-1}.m^{-2}=Kg.A^{-1}.s^{2}$)
\item $LL(\mu,H)=\ds - \frac{\gamma}{1+\lambda^2} \left ( \mu  \wedge H + \lambda \mu \wedge \left ( \mu \wedge H \right ) \right )$, the LLG equation.
\end{itemize}
The atomistic LLG equation describes the interaction of an atomic spin moment of particle  $i$ with an effective magnetic field, which is obtained from the negative first derivative of the complete spin energy, such that:

\begin{equation}
  \ds H^i_{eff}=-\frac{1}{\mu_s}\frac{\partial E}{\partial S_i}
  \label{Eq.EH}
\end{equation}
where $\mu_s$ is the atomic magnetic moment express in units of Joules/Tesla ($J.T^{-1}$) and $E$ the spin energy (or spin Hamiltonian) expressed in units of Joules ($J$).

The energy spin model encapsulates the essential physic magnetic material at the atomic level with the form

\begin{equation}
  \ds E=E_{z}+E_{exc}+E_{ani}+E_{dip}
  \label{Eq.CE}
\end{equation}


\subsection{Zeeman energy }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The Zeeman energy  modelizes the interaction between the particles of the system and an external applied field. The Zeeman energy of a single spin is


\begin{equation}
        E^i_z=- \mu_s <S_i,H_{ext}>_3   
\label{Eq.Ez_1s}
\end{equation}
where $<,>_3$ is the scalar product in a 3D-space.

Because of the relation \ref{Eq.EH}, the Zeeman field expressed in Tesla unit is:
\begin{equation}
H^i_z=H_{ext}
\label{Eq.Hz}
\end{equation}

The total Zeeman energy of the system is the sum of the Zeeman energy of each spin which verifies also the relation \ref{Eq.EH}:
\begin{equation}
        E_z=\sum_i E_z^i=- \sum_i \mu_s <S_i,H_{ext}>_3   
\label{Eq.Ez}
\end{equation}



\subsection{Heisenberg energy }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The Heisenberg or Echange energy is due to the symmetry of the electron wavefunction and the Pauli excusion parameter which governs the orientation of electronic spins in overlapping electron orbitals. Per each spin, the Heisenberg energy is:
\begin{equation}
        E^i_{exc}=- \ds  \sum_{j\neq i} J_{ij} <S_i,S_j>_3
\label{Eq.Eexc_1s}
\end{equation}
where $J_{ij}$ is the exchange interaction between atomic spins of particles $i$ and $j$.\\
The exchange interaction $J_{ij}$ can be considered as  isotropic, meaning that the exchange energy of two spins depends only on their relative orientation, not their direction. In that case $J_{ij}$ is real and positive for ferromagnetic materials (the neighboring spins tend to align parallel), negative for antiferromagnetic materials (the neighboring spins tend to align anti-parallel).\\
The exchange interaction $J_{ij}$ can also be considered as anisotropic. in that case $J_{ij}$ is a $3 \times 3$ matrix:
\begin{equation*}
J_{ij}=
\begin{bmatrix}
J_{xx} & J_{xy} & J_{xz} \\
J_{xy} & J_{yy} & J_{yz} \\
J_{xz} & J_{yz} & J_{zz} \\
\end{bmatrix}
\label{Janisotropic}
\end{equation*}
The expression of the exchange energy of a single spin becomes:
\begin{equation}
        E^i_{exc}=- \ds  \sum_{j\neq i} S_i^t J_{ij} S_j
\label{Eq.Eexc.anisotropic_1s}
\end{equation}

Because of the relation \ref{Eq.EH}, the anisotropic field, expressed in units Tesla, at particle $i$ is
\begin{equation}
H^i_{exc}=\ds \frac{1}{\mu_s} \sum_{j\neq i} J_{ij}.S_j
\label{Eq.Hexc}
\end{equation}

The  Heisenberg energy is the sum if the Heisenberg energy of each spin but in order not to take into account twice the energy of the interaction between spins $i$ and $j$, the sum must be divided by 2 and so the relation  \ref{Eq.EH} is also verified:
\begin{equation}
        E_{exc}=\frac{1}{2} \ds  \sum_i E^i_{exc}=- \ds  \frac{1}{2} \sum_i \sum_{j\neq i} J_{ij} <S_i,S_j>_3
\label{Eq.Eexc}
\end{equation}

\subsection{Anisotropy energy }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
While the exchange energy gives rise to magnetic ordering at the atomic level, the thermal stability of a magnetic material is dominated by the magnetic anisotropy, or preference for the atomic moments to align along a preferred spatial direction.
So he anisotropic energy for a spin depends only on the value of the direction of the atomic spin moment and on spatial directions U:
\begin{equation}
E^i_{ani}=k_U. \Phi_U(S_i)
\label{Eq.Eani_1s}
\end{equation}
In order to satisfy the relation  \ref{Eq.EH}, the anisotropic field is :
\begin{equation}
H^i_{ani}= - \frac{k_U}{\mu_s}. \nabla \Phi_U(S_i)
\label{Eq.Hani}
\end{equation}

The total anisotropy energy of the system is the sum of the anisotropy energy of each spin: 
\begin{equation}
E_{ani}=- \sum_i k_U. \Phi_U(S_i)
\label{Eq.Eani_1s}
\end{equation}

\subsubsection{Uniaxial/Planar anisotropic energy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The simplest form of anisotropy is of the uniaxial type where the magnetic moments prefer to align along a single axis $U$. The uniaxial anisotropy energy of one spin is given by
\begin{equation}
        E^i_{ani}=- \ds  k_u  <S_i,U>^2_3
\label{Eq.Eani.uniaxial_1s}
\end{equation}
where $k_u$ is the anisotropy energy per atom.
To satisfy the general equation,  we have $ \Phi_U(S_i)=- <S_i,U>^2_3 $.  
So the anisotropic field, expressed in units Tesla, at particle $i$ is
\begin{equation}
H^i_{ani}=2.\frac{k_u}{\mu_s} <S_i,U>_3 U 
\label{Eq.Hani.uniaxial}
\end{equation}
Note that if $k_u$ is negative, the anisotropy becomes planar : the magnetic moments prefer to align perpendicular to axis $U$. \\


The total anisotropy energy of the system  which verifies the relation   \ref{Eq.EH}  
\begin{equation}
        E_{ani}=\ds  \sum_i E^i_{ani}= - \ds  \sum_i  k_u <S_i,U>^2_3
\label{Eq.Eani.uniaxial}
\end{equation}

\subsubsection{Cubic anisotropic energy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Materials with a cubic crystal structure has three principal directions which energitically are easy, hard and very hard magnetization direction respectively.
The cubic anisotropy energy of a spin is given by
\begin{equation}
E^i_{ani}=\ds  \frac{k_c}{2} \sum_d <S_i,U_d>_3^4
\label{Eq.Eani.cubic_1s}
\end{equation}
where $k_c$ is the cubic anisotropy energy per atom and $U_d$ the anisotropy directions.
To satisfy the general equation, we have $ \Phi_U(S_i)= \frac{1}{2} \ds \sum_d <S_i,U_d>_3^4 $        
The anisotropic field, expressed in units Tesla, at particle $i$ is
\begin{equation}
H^i_{ani}=-\frac{1}{\mu_s} 2.k_c.\sum_d <S_i,U_d>_3^3 U_d
\label{Eq.Hani.cubic}
\end{equation}


The total anisotropy energy of the system  which verifies the relation   \ref{Eq.EH}  
\begin{equation}
        E_{ani}=\ds  \sum_i E^i_{ani}= \ds  \sum_i  \frac{k_c}{2} \sum_d <S_i,U_d>^4_3
\label{Eq.Eani.cubic}
\end{equation}



\subsection{Dipolar magnetic  energy }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The dipoles magnetic interaction modelized the interaction between all other spins. The  dipoles magnetic energy of a spin $i$ is given by
\begin{equation}
        E^i_{dip}=\ds  \frac{\mu_0 \mu_s^2}{4.\pi} \sum_{j \neq i} \frac{<S_i,S_j>_3}{r_{ij}^3} - \frac{3<S_i,r_{ij}>_3<S_j,r_{ij}>_3}{r_{ij}^5} 
\label{Eq.Edip_1s}
\end{equation}
where $r_{ij}=r_i-r_j$ is the direction vector between the particles $i$ and $j$ in unit $m$. It is in fact the potential energy of the magnetic moment at particle $i$ plugged into a dipolar magnetic field generated by $S_j$.
Because of the relation \ref{Eq.EH}, the dipolar interaction field, expressed in units Tesla, at particle $i$ is
\begin{equation}
H^i_{dip}=-\frac{\mu_0 \mu_s}{4.\pi} \sum_{j\neq i} \frac{1}{r_{ij}^3}S_j - \frac{3<S_j,r_{ij}>_3}{r_{ij}^5} r_{ij}  
\label{Eq.Hdip}
\end{equation}


The  total dipolar  energy of the system is the sum if the dipolar  energy of the spin but in order not to take into account twice the energy of the dipolar interaction betwen spins $i$ and $j$, the sum must be divided by 2 and so the relation  \ref{Eq.EH} is also verified:
\begin{equation}
        E_{dip}=\frac{1}{2} \ds  \sum_i E^i_{dip}=\ds \frac{1}{2}. \sum _i  \frac{\mu_0 \mu_s^2}{4.\pi} \sum_{j \neq i} \frac{<S_i,S_j>_3}{r_{ij}^3} - \frac{3<S_i,r_{ij}>_3<S_j,r_{ij}>_3}{r_{ij}^5} 
\label{Eq.Edip}
\end{equation}

